#######################
####   Zadanie 6   ####
#######################

1)Zaimplementuj wzorzec fabryka abstrakcyjna na podstawie przedstawionego schematu uml.png.
Dla uproszczenia przyjmij, że konkretne implementacje kół (Wheels.java) i pozostałych podzespołów po wywołaniu
ich metod wypisują na ekran charakterystyczne dla nich output, np:
new BigChasis().showMaxLoad() -> "Max load 3,5 tonnes!"

2)Stwórz klase PartsProvider, ze statyczną metodą:
public static void main(String... args)
która odczytuje z pliku properties wartość "mode.big" jako true/false i na tej podstawie tworzy konkretną implementację
AbstractCarPartsFactory, tworzy wszystkie podzespoły i wywołuje implementowane przez nie metody (Wheels, Chassis, Body)

Jak stworzyć / odczytać plik properties:
https://www.mkyong.com/java/java-properties-file-examples/