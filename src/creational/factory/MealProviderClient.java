package creational.factory;

import java.util.EnumMap;
import java.util.Scanner;

public class MealProviderClient {
    public static void main(String... args) {
        MealProviderClient mealProviderClient = new MealProviderClient();
        mealProviderClient.serveBurgers();
    }

    private EnumMap<BurgerType, BurgerFactory> availableFactories;
    private BurgerFactory choosenFactory;

    private void serveBurgers(){
        while (true) {
            System.out.println("Wybierz typ burgera:");
            System.out.println("Z - zwykly z miesem");
            System.out.println("W - wegetarianski");
            System.out.println("K - dla dzieci");
            Scanner scanner = new Scanner(System.in);
            String choice = scanner.next();

            // TODO : wybierz odpowiedni typ burgera poprzez przekazanie wyboru klienta do fabryki
            // choosenFactory = ?

            // TODO : przekaż burgera do metody consume ze stworzonego Singletona

        }
    }

    public MealProviderClient() {
        // TODO : Uzupełnij mapę fabryk
    }
}
