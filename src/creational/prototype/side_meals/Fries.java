package creational.prototype.side_meals;

public class Fries {
    private boolean fried = false;

    public String toString(){
        String status = fried ? "Fried " : "Raw ";
        return status + "Fries!";
    }

    void setFried() {
        this.fried = true;
    }

    public boolean getFried(){
        return this.fried;
    }
}
