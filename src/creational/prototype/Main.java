package creational.prototype;

import creational.prototype.side_meals.Fries;
import creational.prototype.side_meals.FryingOil;

public class Main {
    public static void main(String... args) {
        // TODO: Skróć proces tworzenia 10 porcji frytek
        for (int i = 0; i <10; i++) {
            FryingOil fryingOil = new FryingOil();
            Fries fries = new Fries();
            fryingOil.warmUp();
            fryingOil.makeFries(fries);
            consume(fries);
        }

    }

    private static void consume(Fries f) {
        System.out.println(f);
    }
}