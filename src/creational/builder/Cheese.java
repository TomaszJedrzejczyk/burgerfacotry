package creational.builder;

public class Cheese {
    private String type;
    private int slices;

    public Cheese(String type, int slices) {
        this.type = type;
        this.slices = slices;
    }

    public Cheese(String type) {
        this(type, 1);
    }
}
