package creational.builder;

public class Lettuce {
    private int ammount;

    public Lettuce() {
        this.ammount = 1;
    }

    public Lettuce(int slices) {
        this.ammount = slices;
    }

    public int getAmmount() {
        return ammount;
    }

    public void setAmmount(int slices) {
        this.ammount = slices;
    }
}
