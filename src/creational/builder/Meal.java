package creational.builder;

public interface Meal {
    public void eat();
}
