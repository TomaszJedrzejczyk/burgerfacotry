package creational.builder;

public enum Sauce {
    KETCHUP, MUSTARD, BBQ, SPICY
}
