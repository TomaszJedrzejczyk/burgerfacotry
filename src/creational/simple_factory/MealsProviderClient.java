package creational.simple_factory;

import java.util.Scanner;

public class MealsProviderClient {

    public static void main(String[] args) {
        while (true) {
            System.out.println("Wybierz typ burgera:");
            System.out.println("Z - zwykly z miesem");
            System.out.println("W - wegetarianski");
            System.out.println("S - cheeseburger");
            Scanner scanner = new Scanner(System.in);
            String choice = scanner.next();

            // TODO : wybierz odpowiedni typ burgera poprzez przekazanie wyboru klienta do fabryki

            // TODO : przekaż burgera do metody consume ze stworzonego Singletona

        }

    }
}
