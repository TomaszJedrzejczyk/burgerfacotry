#######################
####   Zadanie 7   ####
#######################

Załóżmy, że jesteś odpowiedzialny za stworzenie interfejsu komunikacji z łazikiem (MarsRover.class)
a kontrolą naziemną (EarthControl.class), która w naszym przypadku będzie klientem zewnętrznym. Wiążę się to z potrzebą
ograniczenia możliwości wykonywania pewnych komend takich jak na przykład "self-destruct".

1) Zaimplementuj tak obiekt MarsProxy.class, aby przekazywał do obiektu MarsRover tylko te komendy, które wpisują się w schemat:
"move X", gdzie X to któryś z kierunków świata N,S,E,W. Obiekt proxy ma zostać zrealizowany w sposób obiektowy, a nie klasowy
Hint: pomocne będą wyrażenia regularne.

