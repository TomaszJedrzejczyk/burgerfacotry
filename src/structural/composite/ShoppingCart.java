package structural.composite;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class ShoppingCart {
    List<PurchaseAble> shopping = new ArrayList<>();

    public void addProduct(PurchaseAble product){
        shopping.add(product);
    }

    public BigDecimal getTotalCost(){
        // TODO : Summarize all products costs
        return null;
    }
}
