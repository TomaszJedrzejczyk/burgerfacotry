package structural.composite.products;
import structural.composite.PurchaseAble;

import java.math.BigDecimal;

public class Chocolate implements PurchaseAble {
    @Override
    public BigDecimal getPrice() {
        return new BigDecimal("3");
    }
}

