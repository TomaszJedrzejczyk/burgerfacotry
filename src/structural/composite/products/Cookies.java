package structural.composite.products;

import structural.composite.PurchaseAble;

import java.math.BigDecimal;

public class Cookies implements PurchaseAble {
    @Override
    public BigDecimal getPrice() {
        return new BigDecimal("5");
    }
}
