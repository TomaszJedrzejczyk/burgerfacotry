package structural.flyweight;

public class ColorFactory {

    public static Color retrieveColor(ColorType colorType) {
        Color color;
        if (colorType == ColorType.BLACK) {
            color = new Color(255,255,255);
        } else if (colorType == ColorType.BLUE) {
            color = new Color(0,0,255);
        } else if (colorType == ColorType.RED) {
            color = new Color(255,0,0);
        } else if (colorType == ColorType.GREEN) {
            color = new Color(0,255,0);
        // default WHITE
        } else {
            color = new Color(0, 0, 0);
        }
        return color;
    }
}
