package structural.flyweight;

public enum ColorType
{
    BLACK, WHITE, BLUE, GREEN, RED
}
