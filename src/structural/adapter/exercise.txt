#######################
####   Zadanie 8   ####
#######################

Jak każdy dobry łazik, MarsRover.class posiada silnik o interfejsie (MarsEngine.class),
jednakże nie ma takiej implementacji i trzeba by go napisać od nowa.
Z drugiej strony, na składzie jest juz gotowy silnik SomeoneLeftGoodEngineHere.class, który można by wykorzystać ponownie
(po co pisać znowu to samo tylko inaczej nazwane).
Niestety, interfejs MarsEngine.class został już potwierdzony i korzysta z niego pare innych elementów łazika,
o których jeszcze nie wiemy - a więc nie można go zmienić.

1) Stwórz adapter dla klasy SomeoneLeftGoodEngineHere.class pod interfejs MarsEngine.class.
Przyjmij, że adapter ma być obiektowy, a ilość rpm dopasowana do pojazdu to 3600.
Dla uproszczenia zakładamy, że obrót pojazdu jest wykonywany poprzez zwykła metodę i generalnie nie spodziewamy się,
że któryś z procesów się nie powiedzie.