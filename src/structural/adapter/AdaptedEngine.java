package structural.adapter;

public class AdaptedEngine implements MarsEngine {
    @Override
    public void setDirecttion(Direction goal) {

    }

    @Override
    public void driveForward(int sec) {

    }

    @Override
    public void driveBackward(int sec) {

    }
}
