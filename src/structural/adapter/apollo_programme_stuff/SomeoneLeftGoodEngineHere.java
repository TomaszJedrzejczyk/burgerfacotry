package structural.adapter.apollo_programme_stuff;

import structural.adapter.Direction;

public final class SomeoneLeftGoodEngineHere {

    public final void rotateVehicle(Direction direction) {
        System.out.println("Rotating vehicle to "+direction.toString());
        try {
            Thread.sleep(1000);
            System.out.println("Vehicle rotated to "+direction.toString());
        } catch (InterruptedException e) {
            System.out.println("Rotate interrupted!");
        }

    }

    public final void work(boolean goForward, int rpm, int seconds) {
        StringBuilder sb = new StringBuilder();
        sb.append(goForward? "Going forward " : "Going backward ");
        sb.append(" at rpm = " + rpm + " for " + seconds + " seconds!");
        try {
            Thread.sleep(seconds*1000);
        } catch (InterruptedException e) {
            System.out.println("Drive interrupted!");
        }
    }

}
