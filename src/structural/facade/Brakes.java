package structural.facade;

public class Brakes {
    public void stopTheVehicle(){
        System.out.println("Vehicle stopped using brakes!");
    }
}
